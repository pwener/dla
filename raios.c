#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(){
	int dimensao = 0, n=0, direcao=0, xL, xR, yU, yD;
	printf("\nEntre com a dimensão da grade: ");
	scanf("%d", &dimensao);
	int grade[dimensao][dimensao];
	FILE *file = fopen("coordenadas.dat", "w");
	srand(time(NULL));
	int x, y;	
	for (x=0; x<dimensao; x++)
	    for (y=0; y<dimensao; y++)
	        grade[x][y] = 0;	

	printf("\nEscolha uma linha na grade: ");
	scanf("%d", &x);
	printf("\nEscolha uma coluna na grade: ");
	scanf("%d", &y);	
	grade[x][y] = 1;	

	while(n <= (10*dimensao)){		
		if (x==0) xL=dimensao-1;
        	else xL = x-1;
		if (x==dimensao-1) xR = 0;
		else xR = x+1;
        	if (y==0) yU = dimensao -1;
        	else yU = y-1;
        	if (y==dimensao-1) yD = 0; 
		else yD = y +1;
		if (grade[xL][y] == 1 || grade[xR][y]== 1 || grade[x][yU]== 1 || grade[x][yD]== 1) {
                	n++;
                	grade[x][y] = 1;
                	while(1){
  	    			x = rand()% dimensao;
  	    			y = rand()% dimensao;
 	    			if (grade[x][y] == 0) break;
	    		}
			continue;
        	}
        	else {
        	      	direcao = rand() % 4;
               		switch(direcao){
               		case 0:
				if(x<dimensao-1) 
                   			x++;
                   			break;
               		case 1:
				if(y<dimensao-1)
                    			y++;
                    			break;
               		case 2:
				if(x>0)
                    			x--;
                   			break;
               		case 3:
				if(y>0)
                    			y--;
                    			break;
               		}
       		}
	}
	int i, j = 0;
	for(i = 0; i < dimensao; i++){
		for(j = 0; j < dimensao; j++){
			if(grade[i][j]==1)
				fprintf(file,"%d %d\n", i, j);
		}
	}
	fclose(file);	
	return 0;
}
